import { IContractState } from "./modules/contracts/types";
import { IPartState } from "./modules/parts/types";

export interface IState {
  contractsList: IContractState,
  partsList: IPartState
}