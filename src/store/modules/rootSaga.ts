import { all } from 'redux-saga/effects'

import contracts from './contracts/sagas'
import parts from './parts/sagas'

export default function* rootSaga() {
  yield all([
    contracts,
    parts
  ])
}