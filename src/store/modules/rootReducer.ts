import { combineReducers } from "redux";

import contractsList from './contracts/reducer'
import partsList from './parts/reducer'

export default combineReducers({
  contractsList,
  partsList
})