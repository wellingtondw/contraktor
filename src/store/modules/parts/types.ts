export enum ActionTypes {  
  listPartsRequest = 'LIST_PARTS_REQUEST',
  listPartsSuccess = 'LIST_PARTS_SUCCESS',
  listPartsFailure = 'LIST_PARTS_FAILURE',
  addNewPartRequest = 'ADD_NEW_PART_REQUEST',
  addNewPartSuccess = 'ADD_NEW_PART_SUCCESS',
  addNewPartFailure = 'ADD_NEW_PART_FAILURE',
  removePartRequest = 'REMOVE_PART_REQUEST',
  removePartSuccess = 'REMOVE_PART_SUCCESS',
  removePartFailure = 'REMOVE_PART_FAILURE',
  listPartDetailsRequest = 'LIST_PART_DETAILS_REQUEST',
  listPartDetailsSuccess = 'LIST_PART_DETAILS_SUCCESS',
  listPartDetailsFailure = 'LIST_PART_DETAILS_FAILURE',
  editPartDetailsRequest = 'EDIT_PART_DETAILS_REQUEST',
  editPartDetailsSuccess = 'EDIT_PART_DETAILS_SUCCESS',
  editPartDetailsFailure = 'EDIT_PART_DETAILS_FAILURE',
}

export interface IPart {
  name: string;
  lastName: string;
  email: string;
  cpf: string;
  phone: string;
  contractId?: string;
}

export interface IPartList extends IPart {
  id: string;
}

export interface IErrors {
  name?: string;
  lastName?: string;
  email?: string;
  cpf?: string;
  phone?: string;
  message?: string;
  contractId?: string;
}

export interface IPartState {
  parts: IPartList[];
  partDetails?: IPart;
  loading: boolean;
  saving: boolean;
  editing: boolean;
  deleting: boolean;
  
  errors?: IErrors;
}