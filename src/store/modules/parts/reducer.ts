import { Reducer } from "redux";
import { IPartState, ActionTypes, IPart } from './types'

const INITIAL_STATE: IPartState = {
  parts: [],
  loading: false,
  saving: false,
  editing: false,
  deleting: false,
  partDetails: {} as IPart
}

const parts: Reducer<IPartState> = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case ActionTypes.listPartsRequest: {
      return {
        ...state,
        loading: true,
        partDetails: {} as IPart,
        errors: {}
      }        
    }
    case ActionTypes.listPartsSuccess: {
      const { parts } = action.payload

      return {
        ...state,
        loading: false,
        parts
      }        
    }
    case ActionTypes.listPartsFailure: {      
      return {
        ...state,
        loading: false,
        errors: {
          message: 'Erro ao listar as partes do contrato'
        }
      }        
    }
    case ActionTypes.addNewPartRequest: {
      return {
        ...state,
        saving: true,
      }        
    }
    case ActionTypes.addNewPartSuccess: {
      return {
        ...state,
        saving: false,
        errors: {}
      }       
    }
    case ActionTypes.addNewPartFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        saving: false,
        errors: {
          ...errors,
          message: 'Erro ao inserir nova parte'
        }
      }        
    }
    case ActionTypes.removePartRequest: {
      return {
        ...state,
        deleting: true,
        success: false,
      }        
    }
    case ActionTypes.removePartSuccess: {
      const { parts } = action.payload 
      return {
        ...state,
        deleting: false,
        parts,
        errors: {}
      }       
    }
    case ActionTypes.removePartFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        deleting: false,
        errors: {
          ...errors,
          message: 'Erro ao excluir contrato'
        }
      }        
    }
    case ActionTypes.listPartDetailsRequest: {
      return {
        ...state,
        loading: true,
        partDetails: {} as IPart,
        errors: {}
      } 
    }
    case ActionTypes.listPartDetailsSuccess: {
      const { part } = action.payload 
      
      return {
        ...state,
        loading: false,
        partDetails: part,
        errors: {}
      }       
    }
    case ActionTypes.listPartDetailsFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        loading: false,
        errors: {
          ...errors,
          message: 'Erro ao listar detalhes da parte'
        }
      }        
    }
    case ActionTypes.editPartDetailsRequest: {
      return {
        ...state,
        editing: true,
        errors: {}
      } 
    }
    case ActionTypes.editPartDetailsSuccess: {
      return {
        ...state,
        editing: false,
        errors: {}
      }       
    }
    case ActionTypes.editPartDetailsFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        editing: false,
        errors: {
          ...errors,
          message: 'Erro ao editar a parte'
        }
      }        
    }
    default: {
      return state
    }
  }
}

export default parts