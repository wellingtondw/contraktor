import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import api from '../../../services/axios/api'
import {
  addNewPartFailure, 
  addNewPartRequest, 
  addNewPartSuccess, 
  editPartDetailsFailure, 
  editPartDetailsRequest, 
  editPartDetailsSuccess, 
  listPartDetailsFailure, 
  listPartDetailsRequest, 
  listPartDetailsSuccess, 
  listPartsFailure, 
  listPartsSuccess,
  removePartFailure,
  removePartRequest,
  removePartSuccess, 
} from './actions'
import { ActionTypes, IPart } from './types'
import * as Yup from 'yup';
import getValidationErrors from '../../../utils/getValidationsErrors';
import { IState } from '../../types';
import { validatePart, formatPart } from '../../../utils/parts';

type AddNewPart = ReturnType<typeof addNewPartRequest>
type RemovePart = ReturnType<typeof removePartRequest>
type ListPartDetails = ReturnType<typeof listPartDetailsRequest>
type EditPartDetails = ReturnType<typeof editPartDetailsRequest>

function* listParts() {
  try {
    const { data }  = yield call(api.get, 'parts')

    yield put(listPartsSuccess(data.parts))
  } catch(error) {
    yield put(listPartsFailure())
  }
}

function* addNewPart({ payload }: AddNewPart) {
  try {
    const { part } = payload

    const validatedPart: IPart = yield validatePart(part)

    const { formattedPart, contractId } = formatPart(validatedPart)

    yield call(api.post, `parts/${contractId}`, formattedPart)
    yield put(addNewPartSuccess())
  } catch(error) {
    if (error instanceof Yup.ValidationError) {
      const errors = getValidationErrors(error);
      
      yield put(addNewPartFailure(errors))
    } else {
      yield put(addNewPartFailure(error))
    }
  }
}

function* removePart({ payload }: RemovePart) {
  try {
    const { partId } = payload

    const newPartsArray: IPart[] = yield select((state: IState) => {
      return state.partsList.parts.filter(part => part.id !== partId)
    })

    yield call(api.delete, `parts/${partId}`)

    yield put(removePartSuccess(newPartsArray))
  } catch(error) {
    yield put(removePartFailure(error))
  }
}

function* listPartDetails({ payload }: ListPartDetails) {
  try {
    const { partId } = payload

    const { data } = yield call(api.get, `parts/${partId}`)

    yield put(listPartDetailsSuccess(data.part))
  } catch(error) {
    yield put(listPartDetailsFailure(error))
  }
}

function* editPartDetails({ payload }: EditPartDetails) {
  try {
    const { part } = payload

    const formattedPart: IPart = yield validatePart(part)

    yield call(api.patch, `parts/${part.id}`, formattedPart)

    yield put(editPartDetailsSuccess())
  } catch(error) {
    if (error instanceof Yup.ValidationError) {
      const errors = getValidationErrors(error);
      
      yield put(editPartDetailsFailure(errors))
    } else {
      yield put(editPartDetailsFailure(error))
    }
  }
}

export default all([
  takeLatest(ActionTypes.listPartsRequest, listParts),
  takeLatest(ActionTypes.addNewPartRequest, addNewPart),
  takeLatest(ActionTypes.removePartRequest, removePart),
  takeLatest(ActionTypes.listPartDetailsRequest, listPartDetails),
  takeLatest(ActionTypes.editPartDetailsRequest, editPartDetails),
])