import { ActionTypes, IErrors, IPart, IPartList } from "./types";

//LIST PARTS
export const listPartsRequest = () => {
  return {
    type: ActionTypes.listPartsRequest    
  }
}

export const listPartsSuccess = (parts: IPartList[]) => {
  return {
    type: ActionTypes.listPartsSuccess,
    payload: {
      parts
    }
  }
}

export const listPartsFailure = () => {
  return {
    type: ActionTypes.listPartsFailure     
  }
}

//ADD NEW PART
export const addNewPartRequest = (part: IPartList) => {
  return {
    type: ActionTypes.addNewPartRequest,
    payload: {
      part
    }
  }
}

export const addNewPartSuccess = () => {
  return {
    type: ActionTypes.addNewPartSuccess
  }
}

export const addNewPartFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.addNewPartFailure,
    payload: {
      errors
    }
  }
}

//REMOVE PART
export const removePartRequest = (partId: string) => {
  return {
    type: ActionTypes.removePartRequest,
    payload: {
      partId
    }
  }
}

export const removePartSuccess = (parts: IPart[]) => {
  return {
    type: ActionTypes.removePartSuccess,
    payload: {
      parts
    }
  }
}

export const removePartFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.removePartFailure,
    payload: {
      errors
    }
  }
}

//LIST PART DETAILS
export const listPartDetailsRequest = (partId: string) => {
  return {
    type: ActionTypes.listPartDetailsRequest,
    payload: {
      partId
    }
  }
}

export const listPartDetailsSuccess = (part: IPartList) => {
  return {
    type: ActionTypes.listPartDetailsSuccess,
    payload: {
      part
    }
  }
}

export const listPartDetailsFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.listPartDetailsFailure,
    payload: {
      errors
    }
  }
}


//EDIT PART DETAILS
export const editPartDetailsRequest = (part: IPartList) => {
  return {
    type: ActionTypes.editPartDetailsRequest,
    payload: {
      part
    }
  }
}

export const editPartDetailsSuccess = () => {
  return {
    type: ActionTypes.editPartDetailsSuccess
  }
}

export const editPartDetailsFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.editPartDetailsFailure,
    payload: {
      errors
    }
  }
}