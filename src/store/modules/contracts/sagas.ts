import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import api from '../../../services/axios/api'
import { 
  addNewContractFailure, 
  addNewContractRequest, 
  addNewContractSuccess, 
  editContractDetailsFailure, 
  editContractDetailsRequest, 
  editContractDetailsSuccess, 
  listContractDetailsFailure, 
  listContractDetailsRequest, 
  listContractDetailsSuccess, 
  listContractsFailure, 
  listContractsSuccess,
  removeContractFailure,
  removeContractRequest,
  removeContractSuccess, 
} from './actions'
import { ActionTypes, IContract } from './types'
import * as Yup from 'yup';
import getValidationErrors from '../../../utils/getValidationsErrors';
import { IState } from '../../types';
import { validateContract } from '../../../utils/contracts';

type AddNewContract = ReturnType<typeof addNewContractRequest>
type RemoveContract = ReturnType<typeof removeContractRequest>
type ListContractDetails = ReturnType<typeof listContractDetailsRequest>
type EditContractDetails = ReturnType<typeof editContractDetailsRequest>

function* listContracts() {
  try {
    const { data }  = yield call(api.get, 'contracts')

    yield put(listContractsSuccess(data.contracts))
  } catch(error) {
    yield put(listContractsFailure())
  }
}

function* addNewContract({ payload }: AddNewContract) {
  try {
    const { contract, file } = payload

    const validatedContract: IContract = yield validateContract({ ...contract, contractFile: file })

    //This part was commented because the fake api I was using does not allow 
    // the upload of files, however I implemented it nonetheless, missing only 
    // this step to send to the backend
    
    // const formattedContract = new FormData()
    //       formattedContract.append('title', validatedContract.title)
    //       formattedContract.append('startDate', validatedContract.startDate)
    //       formattedContract.append('dueDate', validatedContract.dueDate)
    //       formattedContract.append('contractFile', validatedContract.contractFile)

    // yield call(api.post, 'contracts', formattedContract, {
    //   headers: {
    //     'Content-Type':'multipart/form-data'
    //   }
    // })

    yield call(api.post, 'contracts', validatedContract)
    yield put(addNewContractSuccess())
  } catch(error) {
    if (error instanceof Yup.ValidationError) {
      const errors = getValidationErrors(error);
      
      yield put(addNewContractFailure(errors))
    } else {
      yield put(addNewContractFailure(error))
    }
  }
}


function* removeContract({ payload }: RemoveContract) {
  try {
    const { contractId } = payload

    const newContractsArray: IContract[] = yield select((state: IState) => {
      return state.contractsList.contracts.filter(contract => contract.id !== contractId)
    })

    yield call(api.delete, `contracts/${contractId}`)

    yield put(removeContractSuccess(newContractsArray))

  } catch(error) {
    yield put(removeContractFailure(error))
  }
}

function* listContractDetails({ payload }: ListContractDetails) {
  try {
    const { contractId } = payload

    const { data } = yield call(api.get, `contracts/${contractId}`)

    const formattedContractDetailsData = {...data.contract, parts: [...data.parts]}

    yield put(listContractDetailsSuccess(formattedContractDetailsData))
  } catch(error) {
    yield put(listContractDetailsFailure(error))
  }
}

function* editContractDetails({ payload }: EditContractDetails) {
  try {
    const { contract, file } = payload

    const validatedContract: IContract = yield validateContract({ ...contract, contractFile: file })

    yield call(api.patch, `contracts/${contract.id}`, validatedContract)

    yield put(editContractDetailsSuccess())
  } catch(error) {
    if (error instanceof Yup.ValidationError) {
      const errors = getValidationErrors(error);
      
      yield put(editContractDetailsFailure(errors))
    } else {
      yield put(editContractDetailsFailure(error))
    }
  }
}

export default all([
  takeLatest(ActionTypes.listContractsRequest, listContracts),
  takeLatest(ActionTypes.addNewContractRequest, addNewContract),
  takeLatest(ActionTypes.removeContractRequest, removeContract),
  takeLatest(ActionTypes.listContractDetailsRequest, listContractDetails),
  takeLatest(ActionTypes.editContractDetailsRequest, editContractDetails),
])