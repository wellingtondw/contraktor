import { IPartList } from "../parts/types";

export enum ActionTypes {  
  listContractsRequest = 'LIST_CONTRACTS_REQUEST',
  listContractsSuccess = 'LIST_CONTRACTS_SUCCESS',
  listContractsFailure = 'LIST_CONTRACTS_FAILURE',
  addNewContractRequest = 'ADD_NEW_CONTRACT_REQUEST',
  addNewContractSuccess = 'ADD_NEW_CONTRACT_SUCCESS',
  addNewContractFailure = 'ADD_NEW_CONTRACT_FAILURE',
  removeContractRequest = 'REMOVE_CONTRACT_REQUEST',
  removeContractSuccess = 'REMOVE_CONTRACT_SUCCESS',
  removeContractFailure = 'REMOVE_CONTRACT_FAILURE',
  listContractDetailsRequest = 'LIST_CONTRACT_DETAILS_REQUEST',
  listContractDetailsSuccess = 'LIST_CONTRACT_DETAILS_SUCCESS',
  listContractDetailsFailure = 'LIST_CONTRACT_DETAILS_FAILURE',
  editContractDetailsRequest = 'EDIT_CONTRACT_DETAILS_REQUEST',
  editContractDetailsSuccess = 'EDIT_CONTRACT_DETAILS_SUCCESS',
  editContractDetailsFailure = 'EDIT_CONTRACT_DETAILS_FAILURE',
}

export interface IContract {
  title: string;
  startDate: string;
  dueDate: string;
  contractFile?: File;
}

export interface IContractList extends IContract {
  id: string;
}

export interface IErrors {
  title?: string;
  startDate?: string;
  dueDate?: string;
  message?: string;
  contractFile?: string;
}

export interface IContractDetails {
  title: string;
  startDate: string;
  dueDate: string;
  parts: IPartList[]
}

export interface IContractState {
  contracts: IContractList[];
  contractDetails: IContractDetails;
  loading: boolean;
  saving: boolean;
  editing: boolean;
  deleting: boolean;
  errors?: IErrors;
}