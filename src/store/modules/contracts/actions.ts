import { ActionTypes, IContract, IContractList, IErrors } from "./types";

//LIST CONTRACTS
export const listContractsRequest = () => {
  return {
    type: ActionTypes.listContractsRequest    
  }
}

export const listContractsSuccess = (contracts: IContractList[]) => {
  return {
    type: ActionTypes.listContractsSuccess,
    payload: {
      contracts
    }
  }
}

export const listContractsFailure = () => {
  return {
    type: ActionTypes.listContractsFailure     
  }
}

//ADD NEW CONTRACT
export const addNewContractRequest = (contract: IContractList, file: File) => {
  return {
    type: ActionTypes.addNewContractRequest,
    payload: {
      contract,
      file
    }
  }
}

export const addNewContractSuccess = () => {
  return {
    type: ActionTypes.addNewContractSuccess
  }
}

export const addNewContractFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.addNewContractFailure,
    payload: {
      errors
    }
  }
}

//REMOVE CONTRACT
export const removeContractRequest = (contractId: string) => {
  return {
    type: ActionTypes.removeContractRequest,
    payload: {
      contractId
    }
  }
}

export const removeContractSuccess = (contracts: IContract[]) => {
  return {
    type: ActionTypes.removeContractSuccess,
    payload: {
      contracts
    }
  }
}

export const removeContractFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.removeContractFailure,
    payload: {
      errors
    }
  }
}

//LIST CONTRACT DETAILS
export const listContractDetailsRequest = (contractId: string) => {
  return {
    type: ActionTypes.listContractDetailsRequest,
    payload: {
      contractId
    }
  }
}

export const listContractDetailsSuccess = (contract: IContractList) => {
  return {
    type: ActionTypes.listContractDetailsSuccess,
    payload: {
      contract
    }
  }
}

export const listContractDetailsFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.listContractDetailsFailure,
    payload: {
      errors
    }
  }
}


//EDIT CONTRACT DETAILS
export const editContractDetailsRequest = (contract: IContractList, file: File) => {
  return {
    type: ActionTypes.editContractDetailsRequest,
    payload: {
      contract,
      file
    }
  }
}

export const editContractDetailsSuccess = () => {
  return {
    type: ActionTypes.editContractDetailsSuccess
  }
}

export const editContractDetailsFailure = (errors: IErrors) => {
  return {
    type: ActionTypes.editContractDetailsFailure,
    payload: {
      errors
    }
  }
}