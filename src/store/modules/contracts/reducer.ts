import { Reducer } from "redux";
import { IContractState, ActionTypes, IContractDetails } from './types'

const INITIAL_STATE: IContractState = {
  contracts: [],
  loading: false,
  saving: false,
  editing: false,
  deleting: false,
  contractDetails: {} as IContractDetails
}

const contracts: Reducer<IContractState> = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case ActionTypes.listContractsRequest: {
      return {
        ...state,
        loading: true,
        contractDetails: {} as IContractDetails,
        errors: {},
      }        
    }
    case ActionTypes.listContractsSuccess: {
      const { contracts } = action.payload

      return {
        ...state,
        loading: false,
        contracts: contracts
      }        
    }
    case ActionTypes.listContractsFailure: {      
      return {
        ...state,
        loading: false,
        errors: {
          message: 'Erro ao listar contratos'
        }
      }        
    }
    case ActionTypes.addNewContractRequest: {
      return {
        ...state,
        loading: true,
        saving: true,
      }        
    }
    case ActionTypes.addNewContractSuccess: {
      return {
        ...state,
        loading: false,
        saving: false,
        errors: {}
      }       
    }
    case ActionTypes.addNewContractFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        loading: false,
        saving: false,
        errors: {
          ...errors,
          message: 'Erro ao inserir novo contrato'
        }
      }        
    }
    case ActionTypes.removeContractRequest: {
      return {
        ...state,
        deleting: true
      }        
    }
    case ActionTypes.removeContractSuccess: {
      const { contracts } = action.payload 
      return {
        ...state,
        deleting: false,
        contracts,
        errors: {}
      }       
    }
    case ActionTypes.removeContractFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        deleting: false,
        errors: {
          ...errors,
          message: 'Erro ao excluir contrato'
        }
      }        
    }
    case ActionTypes.listContractDetailsRequest: {
      return {
        ...state,
        loading: true,
        contractDetails: {} as IContractDetails,
        errors: {}
      } 
    }
    case ActionTypes.listContractDetailsSuccess: {
      const { contract } = action.payload 
      return {
        ...state,
        loading: false,
        contractDetails: contract,
        errors: {}
      }       
    }
    case ActionTypes.listContractDetailsFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        loading: false,
        errors: {
          ...errors,
          message: 'Erro ao listar detalhes do contrato'
        }
      }        
    }
    case ActionTypes.editContractDetailsRequest: {
      return {
        ...state,
        editing: true,
        errors: {}
      } 
    }
    case ActionTypes.editContractDetailsSuccess: {
      return {
        ...state,
        editing: false,
        errors: {}
      }       
    }
    case ActionTypes.editContractDetailsFailure: { 
      const { errors } = action.payload    
      
      return {
        ...state,
        editing: false,
        errors: {
          ...errors,
          message: 'Erro ao editar o contrato'
        }
      }        
    }
    default: {
      return state
    }
  }
}

export default contracts