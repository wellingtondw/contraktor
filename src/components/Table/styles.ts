import styled from 'styled-components';

export const Table = styled.table`
  width: 100%;
`;

export const Head = styled.thead`
  width: 100%;
  text-align: left;
  background-color: #f5f8fa;
  color: #33475b; 
`;

export const Info = styled.p`
  font-size: 2.0rem;
  margin-top: 8px;
  margin-top: 1.6rem;
`;

export const Row = styled.tr`
  &:nth-child(even) {
    background-color: #f5f8fa;
  }
`;

export const TableHead = styled.th`
  font-weight: 600;
  padding: 12px 8px;
  font-size: 1.8rem;

`;

export const Body = styled.tbody``;

export const TableData = styled.td`
  border: 1px solid #f2f2f2;
  padding: 8px;
  color: #222;
  font-size: 1.4rem;

`;