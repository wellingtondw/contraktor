import styled, { css } from 'styled-components';
import { ButtonProps } from '.';

type SButtonProps = Pick<ButtonProps, 'theme'>

const buttonModifiers = {
  primary: {
    background: '#4597ef',
    outline: '#33475b'
  },
  secondary: {
    background: '#33475b',
    outline: '#4597ef'
  },
  danger: {
    background: '#f2545b',
    outline: '#f2545b'
  }
}

export const Button = styled.button<SButtonProps>`
  ${({ theme }: SButtonProps) => css`
    border: none;
    color: #f5f8fa;
    padding: 8px 16px;
    height: 32px;
    border-radius: 4px;
    transition: filter 0.2s linear; 
    display: flex;
    align-items: center;
    justify-content: center;

    ${!!theme && buttonModifiers[theme]}

    &:disabled {
      cursor: not-allowed;
      opacity: 0.8;
    }
  
    &:hover {
      filter: brightness(0.9);
    }
  `}
`;