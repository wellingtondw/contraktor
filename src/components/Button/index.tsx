import React, { ButtonHTMLAttributes } from 'react';
import Loading from '../Loading';

import * as S from './styles';

export type ButtonProps = {
  theme?: 'primary' | 'secondary' | 'danger',
  loading?: boolean;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const Button: React.FC<ButtonProps> = ({ children, theme = 'primary', loading, ...rest }) => {
  return (
    <S.Button {...rest} theme={theme}>
      {loading ? (
        <Loading theme='secondary'/>
      ) : (
        <>
          {children}
        </>
      )}
    </S.Button>
  )
};

export default Button;