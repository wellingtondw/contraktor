
import React, { useEffect, useCallback, useState } from 'react';
import { useHistory } from 'react-router';
import { BiTrashAlt } from 'react-icons/bi';

import { useDispatch } from 'react-redux'
import { IPartList} from '../../store/modules/parts/types';
import { listPartsRequest, removePartRequest } from '../../store/modules/parts/actions';

import Table from '../Table';
import Button from '../Button';
import * as S from './styles';

export type tableDataProps = {
  value: string | React.ReactNode;
};

export type PartsTableProps = {
  partsList: IPartList[];
  loading?: boolean;
  deleting?: boolean;
  handleDelete?: () => void
};

const headers = [
  {
    text: 'Nome',
  },
  {
    text: 'Sobrenome',
  },
  {
    text: 'Email',
  },
  {
    text: '',
  },
];

const PartsTable = ({ partsList, loading, deleting, handleDelete }: PartsTableProps) => {
  const history = useHistory()
  const dispatch = useDispatch()


  const [data, setData] = useState<tableDataProps[][]>([]);

  useEffect(() => {
    dispatch(listPartsRequest())
  }, [dispatch])

  const handleDeletePart = useCallback((id) => () => {
    dispatch(removePartRequest(id))

    handleDelete && handleDelete()
  }, [dispatch, handleDelete])

  const handleFormatTableData = useCallback(() => {
    const formatTableData = partsList.map((part) => {
      return [
        { value: part.name },
        { value: part.lastName },
        { value: part.email },       
        {
          value: (
            <S.ActionsContainer>
              <Button onClick={() => history.push(`/parte/${part.id}`)}>
                Detalhes
              </Button>
              <Button 
                theme='danger' 
                onClick={handleDeletePart(part.id)}
                loading={deleting}
                disabled={deleting}
              >
                <BiTrashAlt size='14' />
              </Button>
            </S.ActionsContainer>
          ),
        },
      ];
    });

    setData(formatTableData);
  }, [partsList, history, handleDeletePart, deleting]);

  useEffect(() => {
    handleFormatTableData();
  }, [handleFormatTableData]);

  return (
    <Table 
      headers={headers} 
      info={'Nenhuma parte encontrada'} 
      data={data} 
      loading={loading}
    />
  )
};

export default PartsTable;