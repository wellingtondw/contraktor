import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 72px;
  display: flex;
  align-items: center;
  box-shadow: 0 1px 10px rgb(151 164 175 / 10%);
`;

export const Logo = styled.img`
  width: 17.6rem;
`;