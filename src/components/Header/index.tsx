import { Link } from 'react-router-dom';

import Logo from '../../assets/images/contraktor-logo.png'

import { Wrapper } from '../../styles/common';
import * as S from './styles';

const Header = () => {
  return (
    <S.Container>
      <Wrapper>
        <Link to='/'>
          <S.Logo src={Logo} alt='contraktor'/>
        </Link>
      </Wrapper>
    </S.Container>
  );
};

export default Header;