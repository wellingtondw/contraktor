import styled from 'styled-components'
import { Container } from '../../components/Input/styles'

export const Form = styled.form`

  > div {
    width: 100%;
    max-width: 480px;
  }

  ${Container} {

    & + p {
      margin-top: 2.4rem;
    }    
  }
`

