import { useCallback, useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { IState } from "../../store/types"
import { IContract, IContractState } from "../../store/modules/contracts/types"

import { formatDate } from "../../utils/date"
import { fileFormatsSupported } from '../../utils/contracts'

import Input from "../Input"
import { ErrorMessage, SendButton, Title } from "../../styles/common"
import * as S from './styles'
import Loading from "../Loading"

type FormDataState = {
  title: string;
  startDate: string;
  dueDate: string;
}

type ContractFormProps = {
  title: string;
  buttonText: string;
  handleSubmit: (formData: IContract, file: File) => void
}

const ContractForm = ({ title, buttonText, handleSubmit }: ContractFormProps) => {
  const { errors, loading, contractDetails, editing, saving } = useSelector<IState, IContractState>(state => state.contractsList)

  const [formData, setFormData] = useState<FormDataState>({
      title: '',
      startDate: '',
      dueDate: ''
  })
  const [file, setFile] = useState({} as File)

  useEffect(() => {
    if(contractDetails?.title) {      
      setFormData({ 
        ...contractDetails,
        startDate: formatDate(contractDetails.startDate),
        dueDate: formatDate(contractDetails.dueDate)
      })
    }

  }, [contractDetails])

  const handleChangeFormData = useCallback((event: React.ChangeEvent<HTMLInputElement>, field) => {
    setFormData({
      ...formData,
      [field]: event.target.value
    })
  }, [formData])

  const handleUploadFile = useCallback((event: React.ChangeEvent<HTMLInputElement>, field) => {
    const uploadedFile = event.target.files && event.target.files[0]

    if(uploadedFile && fileFormatsSupported.includes(uploadedFile.type)) {
      setFile(uploadedFile)
    }
  }, [])

  const handleSubmitForm = useCallback((event) => {
    event.preventDefault()
    handleSubmit(formData, file)       
  }, [formData, handleSubmit, file])

  return (
    <>
      <Title>{title}</Title>
      {loading && <Loading />}
      {errors && <ErrorMessage>{errors?.message}</ErrorMessage>}
      <S.Form onSubmit={handleSubmitForm}>
          <Input 
            type='text' 
            name='title' 
            label='Título do contrato' 
            value={formData.title}
            onChange={(e) => handleChangeFormData(e, 'title')}
            error={errors && errors['title']}
          />
          <Input
            type='text' 
            name='start-date' 
            mask='99/99/9999'
            label='Data de inicio do contrato' 
            value={formData.startDate}
            onChange={(e) => handleChangeFormData(e, 'startDate')}
            error={errors && errors['startDate']}
          />
          <Input 
            type='text' 
            name='due-date' 
            mask='99/99/9999'
            label='Data de término do contrato' 
            value={formData.dueDate}
            onChange={(e) => handleChangeFormData(e, 'dueDate')}
            error={errors && errors['dueDate']}
          />

          <Input 
            type='file' 
            name='file' 
            onChange={(e) => handleUploadFile(e, 'file')}
            error={errors && errors['contractFile']}
          />

          <SendButton loading={editing || saving} theme='secondary'>
            {buttonText}
          </SendButton>
      </S.Form>
    </>
  )
}

export default ContractForm