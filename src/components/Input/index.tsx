import React, {
  InputHTMLAttributes,
  useRef,
  useState,
  useCallback,
} from 'react';
import { IconBaseProps } from 'react-icons';
import { FiAlertCircle } from 'react-icons/fi'
import InputMask from 'react-input-mask'

import { Label } from '../../styles/common';
import * as S from './styles'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  error?: string;
  icon?: React.ComponentType<IconBaseProps>;
  label?: string;
  mask?: string | (string | RegExp)[];
}

const Input: React.FC<InputProps> = ({ name, icon: Icon, mask, label, error, ...rest }) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);

    setIsFilled(!!inputRef.current?.value);
  }, []);

  const renderInput = useCallback(() => {
    if(!!mask) {
      return (
        <InputMask 
          mask={mask}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          {...rest}        
        >
          <input          
            ref={inputRef}
          />
        </InputMask>
      )
    }

    return (
      <input          
        ref={inputRef}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        {...rest} 
      />
    )
  }, [mask, handleInputFocus, handleInputBlur, rest])

  return (
    <>  
      {label && <Label>{label}</Label>}
      <S.Container isErrored={!!error} isFocused={isFocused} isFilled={isFilled}>
        {Icon && <Icon size={20} />}
        {renderInput()}
        {error && (
          <S.Error title={error}>
            <FiAlertCircle color="#f2545b" size={20} />
          </S.Error>
        )}
      </S.Container>
    </>
  );
};

export default Input;