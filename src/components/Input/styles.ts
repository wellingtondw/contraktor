import styled, { css } from 'styled-components';
import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #f5f8fa;
  border-radius: 4px;
  width: 100%;
  display: flex;
  align-items: center;
  border: 1px solid #cbd6e2;
  color: #33475b;
  padding-right: 16px;

  & + div {
    margin-top: 8px;
  }

  ${props =>
    props.isFocused &&
    css`
      border-color: #52a8ec;
    `}

  ${props =>
    props.isFilled &&
    css`
      border-color: #52a8ec;
    `}

  ${props =>
    props.isErrored &&
    css`
      border-color: #f2545b;
    `}

  input {
    flex: 1;
    background: transparent;
    padding: 1.4rem;
    border: 0;
    color: #33475b;
    outline: none;
    font-size: 1.6rem;
  }

  svg {
    margin-right: 16px;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;

  svg {
    margin: 0;
  }

  span {
    background: #f2545b;
    color: #f5f8fa;
    
    &::before {
      border-color: #f2545b transparent;
    }
  }
`;