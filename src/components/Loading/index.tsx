import { BiLoaderAlt } from 'react-icons/bi';

import * as S from './styles'

type LoadingProps = {
  theme?: 'primary' | 'secondary'
}

const Loading = ({ theme = 'primary' }: LoadingProps) => {
  return (
    <S.Container>
      <BiLoaderAlt size='20' color={theme === 'primary' ? '#52a8ec' : '#f5f8fa'}/>
    </S.Container>
  )
}

export default Loading