import { useCallback, useEffect, useState } from "react"

import { useDispatch, useSelector } from "react-redux"
import { IPart, IPartState } from "../../store/modules/parts/types"
import { IState } from "../../store/types"
import { IContractState } from "../../store/modules/contracts/types"
import { listContractsRequest } from '../../store/modules/contracts/actions'

import Loading from "../Loading"
import Input from "../Input"
import { ErrorMessage, Label, SendButton, Title } from "../../styles/common"
import * as S from './styles'

type FormDataState = {
  name: string;
  lastName: string;
  email: string;
  cpf: string;
  phone: string;
  contractId: string;
}

type PartFormProps = {
  title: string;
  buttonText: string;
  handleSubmit: (formData: IPart) => void
}

const PartForm = ({ title, buttonText, handleSubmit }: PartFormProps) => {
  const dispatch = useDispatch()

  const { errors, loading, partDetails, editing, saving } = useSelector<IState, IPartState>(state => state.partsList)
  const { contracts, loading: contractLoading } = useSelector<IState, IContractState>(state => state.contractsList)

  const [formData, setFormData] = useState<FormDataState>({
    name: '',
    lastName: '',
    email: '',
    cpf: '',
    phone: '',
    contractId: ''
  })

  useEffect(() => {
    dispatch(listContractsRequest())
  }, [dispatch])

  useEffect(() => {  
    if(partDetails?.name) {   
      return setFormData({ 
        ...partDetails,
        contractId: partDetails.contractId || contracts[0].id
      })
    }

    setFormData({
      ...formData,
      contractId: contracts[0]?.id
    })
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [partDetails, contracts])

  const handleChangeFormData = useCallback((event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>, field) => {
    setFormData({
      ...formData,
      [field]: event.target.value
    })
  }, [formData])

  const handleSubmitForm = useCallback((event) => {
    event.preventDefault()

    handleSubmit(formData)       
  }, [formData, handleSubmit])

  if(contractLoading) {
    return <Loading />
  }

  return (
    <>
      <Title>{title}</Title>
      {loading && <p>Carregando</p>}
      {errors && <ErrorMessage>{errors?.message}</ErrorMessage>}
      <S.Form onSubmit={handleSubmitForm}>
          <Input 
            type='text' 
            name='name' 
            label='Nome' 
            value={formData.name}
            onChange={(e) => handleChangeFormData(e, 'name')}
            error={errors && errors['name']}
          />
          <Input
            type='text' 
            name='lastname' 
            label='Sobrenome' 
            value={formData.lastName}
            onChange={(e) => handleChangeFormData(e, 'lastName')}
            error={errors && errors['lastName']}
          />
          <Input 
            type='email' 
            name='email' 
            label='E-mail' 
            value={formData.email}
            onChange={(e) => handleChangeFormData(e, 'email')}
            error={errors && errors['email']}
          />
          <Input 
            type='text' 
            name='cpf' 
            label='CPF' 
            mask='999.999.999-99'
            value={formData.cpf}
            onChange={(e) => handleChangeFormData(e, 'cpf')}
            error={errors && errors['cpf']}
          />
          <Input 
            type='text' 
            name='phone' 
            label='Telefone' 
            mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
            value={formData.phone}
            onChange={(e) => handleChangeFormData(e, 'phone')}
            error={errors && errors['phone']}
          />

          {errors && errors['contractId'] && <ErrorMessage>{errors['contractId']}</ErrorMessage>}
          <Label>Selecione um contrato</Label>
          <select 
            name='contracts' 
            id='contracts' 
            value={formData.contractId || contracts[0]?.id}
            onChange={e => handleChangeFormData(e, 'contractId')}
          >
            {contracts.map((contract) => (
              <option key={contract.id} value={contract.id}>{contract.title}</option>
            ))}
          </select>

          <SendButton theme='secondary' loading={editing || saving}>
            {buttonText}
          </SendButton>
      </S.Form>
    </>
  )
}

export default PartForm