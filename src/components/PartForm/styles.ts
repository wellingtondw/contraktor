import styled from 'styled-components'
import { Container } from '../../components/Input/styles'

export const Form = styled.form`

  > div {
    width: 100%;
    max-width: 480px;
  }

  ${Container} {

    & + p {
      margin-top: 2.4rem;
    }    
  }

  select {
    width: 100%;
    max-width: 480px;
    padding: 1.4rem;
    border-radius: 0.4rem;
    display: block;
    margin: 0 0 1.6rem;
    background: #f5f8fa;
    border: 1px solid #cbd6e2;
    outline: 0px;

    option {
      font-size: 1.4rem;      
    }
  }
`