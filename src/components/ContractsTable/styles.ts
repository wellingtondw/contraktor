import styled from 'styled-components';

export const ActionsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  button {
    margin-right: 0.8rem;
  }
`;