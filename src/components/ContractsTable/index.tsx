
import React, { useEffect, useCallback, useState } from 'react';
import { useHistory } from 'react-router';
import { BiTrashAlt } from 'react-icons/bi';

import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../store/types';
import { IContractState } from '../../store/modules/contracts/types';
import { listContractsRequest, removeContractRequest } from '../../store/modules/contracts/actions';

import { formatDate } from '../../utils/date';

import Table from '../Table';
import Button from '../Button';
import * as S from './styles';

export type tableDataProps = {
  value: string | React.ReactNode;
};


const headers = [
  {
    text: 'Título',
  },
  {
    text: 'Início do Contrato',
  },
  {
    text: 'Término do Contrato',
  },
  {
    text: '',
  },
];


const ContractTable = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const { contracts, loading, deleting } = useSelector<IState, IContractState>(state => state.contractsList)

  const [data, setData] = useState<tableDataProps[][]>([]);

  useEffect(() => {
    dispatch(listContractsRequest())
  }, [dispatch])


  const handleFormatTableData = useCallback(() => {
    const formatTableData = contracts.map((contract) => {
      return [
        { value: contract.title },
        { value: formatDate(contract.startDate) },
        { value: formatDate(contract.dueDate) },
        {
          value: (
            <S.ActionsContainer>
              <Button onClick={() => history.push(`/contrato/${contract?.id}`)}>
                Detalhes
              </Button>
              <Button 
                theme='danger' 
                onClick={() => dispatch(removeContractRequest(contract.id))}
                loading={deleting}
                disabled={deleting}
              >
                <BiTrashAlt size='14' />
              </Button>
            </S.ActionsContainer>
          ),
        },
      ];
    });

    setData(formatTableData);
  }, [contracts, history, dispatch, deleting]);

  useEffect(() => {
    handleFormatTableData();
  }, [handleFormatTableData]);

  return (
    <Table 
      headers={headers} 
      info={'Nenhum contrato encontrado'} 
      data={data} 
      loading={loading}
    />
  )
};

export default ContractTable;