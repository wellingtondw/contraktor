import styled from 'styled-components'
import Button from '../components/Button'

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1110px;
  margin: 0 auto;
  padding: 4.0rem 1.6rem;
`;

export const ErrorMessage = styled.p`
  font-size: 1.8rem;
  color: #f2545b;
  margin: 2.0rem 0;
`

export const Title = styled.h2`
  font-size: 3.2rem;
  font-weight: 600;
  border-bottom: 1px solid #f5f8fa;
  padding-bottom: 1.6rem;
  margin-bottom: 2.0rem;
`

export const SendButton = styled(Button)`
  margin-top: 1.6rem;
  height: 4.0rem;
`

export const Label = styled.p`
  font-size: 1.6rem;
  color: #33475b;
  margin: 0.4rem 0;
`