import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux'

import { makeServer } from './services/mirage'

import GlobalStyles from './styles/global'
import store from './store';
import Routes from './routes';

import Header from './components/Header';

if(process.env.NODE_ENV !== 'production') {
  makeServer()
}

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Header />
        <Routes />
      </Provider>
      <GlobalStyles />
    </Router>
  );
}

export default App;
