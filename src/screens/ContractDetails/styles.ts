import styled from 'styled-components'

export const Container = styled.div``

export const PartsContainer = styled.div`
  margin-top: 4rem;

  h1 {
    font-size: 3.2rem;
    font-weight: 600;
    border-bottom: 1px solid #f5f8fa;
    padding-bottom: 1.6rem;
    margin-bottom: 2.0rem;
  }
`