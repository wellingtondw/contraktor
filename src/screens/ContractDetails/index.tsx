import { useCallback, useEffect } from "react"
import { useParams } from 'react-router-dom'

import { useDispatch, useSelector } from 'react-redux'
import { editContractDetailsRequest, listContractDetailsRequest } from "../../store/modules/contracts/actions"
import ContractForm from "../../components/ContractForm"
import { IState } from "../../store/types"
import { Wrapper } from "../../styles/common"
import { IContractState } from "../../store/modules/contracts/types"
import { IPartState } from "../../store/modules/parts/types"

import * as S from './styles'
import PartsTable from "../../components/PartsTable"
import Loading from "../../components/Loading"

type ParamsState = {
  id: string;
}

const ContractDetails = () => {
  const params = useParams<ParamsState>()
  const dispatch = useDispatch()
  const { contractDetails } = useSelector<IState, IContractState>(state => state.contractsList)
  const { deleting } = useSelector<IState, IPartState>(state => state.partsList)

  const { id } = params

  useEffect(() => {
    dispatch(listContractDetailsRequest(id))      
  }, [dispatch, id])

  const handleSubmit = useCallback(async (formData, file) => {
    dispatch(editContractDetailsRequest({ id, ...formData }, file))
  }, [id, dispatch])

  return (
    <Wrapper>
      <>
        <ContractForm title='Editar Contrato' buttonText='Salvar' handleSubmit={handleSubmit}/>
        <S.PartsContainer>
          <h1>Partes relacionadas com o contrato</h1>
          {deleting && <Loading />}
          {contractDetails?.parts && (
            <PartsTable 
              partsList={contractDetails.parts} 
              handleDelete={() => dispatch(listContractDetailsRequest(id))}
            />
          )}
        </S.PartsContainer>
      </>
    </Wrapper>
  )
}

export default ContractDetails
