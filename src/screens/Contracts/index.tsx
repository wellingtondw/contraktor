import { useHistory } from 'react-router';

import ContractsTable from '../../components/ContractsTable'
import { Wrapper } from '../../styles/common';
import * as S from './styles'

const Contracts = () => {
  const history = useHistory()

  return (
    <Wrapper>
      <S.Main>
        <S.ButtonsWrapper>
        <S.PartButton theme='secondary' onClick={() => history.push('/partes')}>
            Partes Cadastradas
          </S.PartButton>

          <div>
            <S.AddNewContractButton theme='secondary' onClick={() => history.push('/novo-contrato')}>
              Adicionar novo contrato
            </S.AddNewContractButton>
            <S.PartButton theme='secondary' onClick={() => history.push('/nova-parte')}>
              Adicionar nova parte
            </S.PartButton>
          </div>
        </S.ButtonsWrapper>
        <ContractsTable />
      </S.Main>
    </Wrapper>
  )
}

export default Contracts
