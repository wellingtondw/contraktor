import styled from 'styled-components';
import { Button } from '../../components/Button/styles';

export const Main = styled.div`
  display: flex;
  flex-direction: column;
`

export const ButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  > div {
    display: flex;
  }
`

export const AddNewContractButton = styled(Button)`
  margin-bottom: 2.0rem;
  height: 4.0rem;
  margin-right: 1.6rem;
`

export const PartButton =  styled(AddNewContractButton)``