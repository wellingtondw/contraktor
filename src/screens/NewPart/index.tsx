
import { useCallback } from 'react';

import { useDispatch } from 'react-redux';
import { addNewPartRequest } from '../../store/modules/parts/actions';

import PartForm from '../../components/PartForm';
import { Wrapper } from '../../styles/common';

const NewPart = () => {
  const dispatch = useDispatch()
 
  const handleSubmit = useCallback((formData) => {
    dispatch(addNewPartRequest(formData)) 
  }, [dispatch])

  return (
    <Wrapper>
      <PartForm title='Adicionar Nova Parte' buttonText='Cadastrar' handleSubmit={handleSubmit} />
    </Wrapper>
  )
}

export default NewPart