import { useHistory } from 'react-router';

import { useSelector } from 'react-redux';
import { IPartState } from '../../store/modules/parts/types';
import { IState } from '../../store/types';

import PartsTable from '../../components/PartsTable'
import { Wrapper } from '../../styles/common';
import * as S from './styles'

const Parts = () => {
  const { parts, loading, deleting } = useSelector<IState, IPartState>(state => state.partsList)

  const history = useHistory()

  return (
    <Wrapper>
      <S.Main>
        <S.ButtonsWrapper>
        <S.PartButton theme='secondary' onClick={() => history.push('/')}>
            Contratos Cadastrados
        </S.PartButton>
          <div>
            <S.AddNewContractButton theme='secondary' onClick={() => history.push('/novo-contrato')}>
              Adicionar novo contrato
            </S.AddNewContractButton>            
            <S.PartButton theme='secondary' onClick={() => history.push('/nova-parte')}>
              Adicionar nova parte
            </S.PartButton>
          </div>
        </S.ButtonsWrapper>
        <PartsTable partsList={parts} deleting={deleting} loading={loading}/>
      </S.Main>
    </Wrapper>
  )
}

export default Parts
