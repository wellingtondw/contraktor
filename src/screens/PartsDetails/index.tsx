import { useCallback, useEffect } from "react"
import { useParams } from 'react-router-dom'

import { useDispatch } from 'react-redux'
import { editPartDetailsRequest, listPartDetailsRequest } from "../../store/modules/parts/actions"

import { Wrapper } from "../../styles/common"
import PartForm from "../../components/PartForm"

type ParamsState = {
  id: string;
}

const PartsDetails = () => {
  const params = useParams<ParamsState>()
  const dispatch = useDispatch()
  const { id } = params

  useEffect(() => {
    dispatch(listPartDetailsRequest(id))      
  }, [dispatch, id])

  const handleSubmit = useCallback(async (formData) => {
    dispatch(editPartDetailsRequest({ id, ...formData }))
  }, [id, dispatch])

  return (
    <Wrapper>
      <PartForm title='Editar Parte' buttonText='Salvar' handleSubmit={handleSubmit}/>
    </Wrapper>
  )
}

export default PartsDetails
