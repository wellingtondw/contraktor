
import { useCallback } from 'react';
import { RouterProps } from 'react-router';

import { useDispatch } from 'react-redux';
import { addNewContractRequest } from '../../store/modules/contracts/actions';

import ContractForm from '../../components/ContractForm';
import { Wrapper } from '../../styles/common';

const NewContract = ({ history }: RouterProps) => {
  const dispatch = useDispatch()
   
  const handleSubmit = useCallback((formData, file) => {
    dispatch(addNewContractRequest(formData, file)) 
  }, [dispatch])

  return (
    <Wrapper>
      <ContractForm title='Adicionar Contrato' buttonText='Cadastrar' handleSubmit={handleSubmit} />
    </Wrapper>
  )
}

export default NewContract