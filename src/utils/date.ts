export const formatDate = (date: string) => {
  return new Date(date).toLocaleDateString('pt-br');
}

export const parseIso = (date: string) => {
  const formatedDate = date.split('/').reverse().join('-')

  return `${formatedDate}T10:20:00Z`
}