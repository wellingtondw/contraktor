import * as Yup from 'yup'
import { IContract } from '../store/modules/contracts/types'

import { parseIso } from './date'

export const fileFormatsSupported = ['application/pdf', 'application/msword']

export const formatContractData = (contract: IContract) => {
  const formattedData = {
    ...contract,
    startDate: parseIso(contract.startDate),
    dueDate: parseIso(contract.dueDate),
  } 

  return formattedData
}

export const validateContract = async (contract: IContract) => {
  try {
    const schema = Yup.object().shape({
      title: Yup.string().min(3, 'O título deve ter no mínimo 3 caracteres.')
      .required('Digite um título para o contrato.'),
      startDate: Yup.date().required('Digite uma data de início do contrato.'),
      dueDate: Yup.date().required('Digite uma data de término do contrato.'),
      contractFile: Yup.mixed().test('contractFile', 'Selecione um arquivo válido', value => fileFormatsSupported.includes(value.type))
    });

    const formatedContractData = formatContractData(contract)

    await schema.validate(formatedContractData, {
      abortEarly: false,
    });

    return formatedContractData
  } catch (error) {
    throw new Yup.ValidationError(error)
  }
}

