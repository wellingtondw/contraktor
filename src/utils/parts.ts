import * as Yup from 'yup'
import { IPart } from '../store/modules/parts/types'

export const formatPart = (part: IPart) => {
  const { contractId } = part

  const formattedPart = {
    ...part
  }
  delete formattedPart.contractId

  return {
    formattedPart,
    contractId  
  }
}

export const validatePart = async (part: IPart) => {
  try {
    const schema = Yup.object().shape({
      name: Yup.string().min(3, 'O nome deve ter no mínimo 3 caracteres.')
      .required('O nome não pode ser vazio.'),
      lastName:  Yup.string().min(3, 'O sobrenome deve ter no mínimo 3 caracteres.')
      .required('O sobrenome não pode ser vazio.'),
      email:  Yup.string().required('O email não pode ser vazio.'),
      cpf:  Yup.string().required('O cpf não pode ser vazio.'),
      phone:  Yup.string().required('O telefone não pode ser vazio.'),
      contractId:  Yup.string().required('Por favor selecione um contrato.'),      
    });

    await schema.validate(part, {
      abortEarly: false,
    });

    return part
  } catch (error) {
    throw new Yup.ValidationError(error)
  }
}

