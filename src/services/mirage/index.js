import { belongsTo, createServer, Factory, hasMany, Model,  } from 'miragejs'
import faker from 'faker'

export function makeServer() {
  const server = createServer({
    models: {
      contract: Model.extend({
        part: hasMany()
      }),
      part: Model.extend({
        contract: belongsTo()
      })
    },
    factories: {
      contract: Factory.extend({
        title(i) {
          return `Contrato ${i + 1}`
        },
        startDate() {
          return faker.date.future(2)
        },
        dueDate() {
          return faker.date.future(4)
        },
        createdAt() {
          return faker.date.recent(10)
        }
      }),
      part: Factory.extend({
        name(i) {
          return faker.name.firstName()
        },
        lastName() {
          return faker.name.lastName()
        },
        email() {
          return faker.internet.email()
        },
        cpf(i) {
          return `111.111.111-1${i}`
        },
        phone() {
          return faker.phone.phoneNumber()
        },
        createdAt() {
          return faker.date.recent(10)
        }
      })
    },
    seeds(server) {
      server.createList('contract', 10)
      server.createList('part', 10)

      let contract = server.create("contract", { 
        title: 'Contrato com relacionamento', 
        startDate: faker.date.future(2),
        dueDate:  faker.date.future(10),
        createdAt: faker.date.recent(10),
      });
      server.create('part', { contract, 
        name: 'José', 
        lastName: 'Silva', 
        email: faker.internet.email(), 
        cpf: '345.543.123-23',
        phone: faker.phone.phoneNumber(),
        createdAt: faker.date.recent(10),
      });
      server.create('part', { contract, 
        name: 'Joseph', 
        lastName: 'Bryan', 
        email: faker.internet.email(), 
        cpf: '123.543.433-12',
        phone: faker.phone.phoneNumber(),
        createdAt: faker.date.recent(10),
      });
    },
    routes() {
      this.namespace = 'api'
      this.timing = 350

      //contracts
      this.get("/contracts", (schema, request) => {
        return schema.all('contract')
      })
      this.post("/contracts", (schema, request) => {
        let attrs = JSON.parse(request.requestBody)
        attrs.id = Math.floor(Math.random() * 100)
        attrs.createdAt = new Date()

        schema.create('contract', attrs)
  
        return { contract: attrs }
      })
      this.patch("/contracts/:id", (schema, request) => {
        let newAttrs = JSON.parse(request.requestBody)
        let id = request.params.id
        let contract = schema.contracts.find(id)
      
        return contract.update(newAttrs)
      })
      this.delete("/contracts/:id", (schema, request) => {
        let id = request.params.id
        
        return schema.contracts.find(id).destroy()
      })

      this.get("/contracts/:id", (schema, request) => {
        let id = request.params.id

        const contract =  schema.contracts.findBy({ id })
        const partsResult = schema.parts.find(contract.partIds)

        const parts = partsResult.models.map(part => {
          return {
            ...part.attrs
          }
        })

        return { contract, parts }
      })

      this.get("/contracts/:id/parts", (schema, request) => {
        let contractId = request.params.id
        let contract = schema.contracts.find(contractId)
      
        return contract.partIds
      })

      //parts
      this.get("/parts", (schema, request) => {
        return schema.all('part')
      })

      this.post("/parts/:contractId", (schema, request) => {
        let attrs = JSON.parse(request.requestBody)
        let { contractId } = request.params
        attrs.id = Math.floor(Math.random() * 100)
        attrs.createdAt = new Date()
        
        const contract = schema.contracts.findBy({ id: contractId })
        schema.create('part', { contract, ...attrs })
          
        return { part: attrs, contract }
      })

      this.get("/parts/:id", (schema, request) => {
        let id = request.params.id

        schema.contracts.find(id, 'partsId')
        const part = schema.parts.findBy({ id })

        return { part }
      })

      this.patch("/parts/:id", (schema, request) => {
        let newAttrs = JSON.parse(request.requestBody)
        let id = request.params.id
        let part = schema.parts.find(id)
      
        return part.update(newAttrs)
      })

      this.delete("/parts/:id", (schema, request) => {
        let id = request.params.id
        
        return schema.parts.find(id).destroy()
      })

      this.namespace = ''
      this.passthrough()
    }
  })

  return server
}