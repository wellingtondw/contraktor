import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Contracts from '../screens/Contracts';
import NewContract from '../screens/NewContract';
import ContractDetails from '../screens/ContractDetails';
import NewPart from '../screens/NewPart';
import Parts from '../screens/Parts';
import PartsDetails from '../screens/PartsDetails';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Contracts} />
    <Route path="/novo-contrato" exact component={NewContract} />
    <Route path="/contrato/:id" exact component={ContractDetails} />
    <Route path="/nova-parte" exact component={NewPart} />
    <Route path="/partes" exact component={Parts} />
    <Route path="/parte/:id" exact component={PartsDetails} />

  </Switch>
);

export default Routes;