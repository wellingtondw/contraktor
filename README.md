# Contraktor Challenge 📖
This repository refers to a Front-end Contraktor application made with React.js <br />
Project developed by Wellington Lima 🚀 <br/><br/>
<a href="https://www.linkedin.com/in/wellington-lima-silva/" target="_blank"> Linkedin </a>
<br />

<a href="https://github.com/wellingtondw" target="_blank"> Github </a>
<br />
<br />

# Tecnologies 🚀
React ⚛️ <br />
Redux/Sagas ⚛️ <br />
Styled Components ⚛️ <br />
React Icons ⚛️ <br />
Typescript ⚛️ <br />

# Result 
<p align="center">
  <img src="https://res.cloudinary.com/dyxcgmvy9/image/upload/v1618770558/Contraktor/home_lqk55l.png" alt='Editar contrato page'/>
</p>

<br />


# Dependencies 
1. NodeJs <code>https://nodejs.org/en/</code>.
1. Yarn <code>https://yarnpkg.com/</code>.

<br />

# How to get this repository? 
To obtain this project, follow the steps:
1. Clone this repository using <code> git clone `git@gitlab.com:wellingtondw/contraktor.git`</code>.
2. Run the <code> yarn </code> command at the root of the cloned project folder to download the dependencies.
3. Run <code> yarn start </code> at the root of the project folder to start the application in development mode.
